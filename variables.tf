#REQUIRED PARAMETERS

variable price_class {
  type        = string
  description = "Clase de precio del cloudfront. PriceClass_All, PriceClass_200, PriceClass_100"
}

variable default_root_object {
  type        = string
  description = "Ruta default a cargar en el bucket"
}

variable origin {
  type        = any
  description = "Configuracion del origin"
}

variable default_cache_behavior {
  type        = any
  description = "Comportamiento por default del cache"
}

#OPTIONAL PARAMETERS

variable ordered_cache_behavior {
  type        = any
  default     = []
  description = "Orden del comportamiento"
}

variable aliases {
  type        = list(string)
  default     = null
  description = "Aliases (CNAMES) para acceder al cloudfront"
}

variable comment {
  type        = string
  default     = ""
  description = "Comentario referente al cloudfront"
}

variable enabled {
  type        = bool
  default     = true
  description = "Habilitar cloudfront"
}


variable is_ipv6_enabled {
  type        = bool
  default     = true
  description = "Habilitar ipv6"
}

variable retain_on_delete {
  type        = bool
  default     = false
  description = "Mantener en eliminacion"

}

variable wait_for_deployment {
  type        = bool
  default     = false
  description = "Esperar por el deploy"
}

variable viewer_certificate {
  type = any
  default = {
    cloudfront_default_certificate = true
    minimum_protocol_version       = "TLSv1"
  }
  description = "Configuracion del certificado"
}

variable create_origin_access_identity {
  type        = bool
  default     = false
  description = "Crear origin access identity"
}

variable origin_access_identities {
  type        = map(string)
  default     = {}
  description = "Origin access identities"
}


variable geo_restriction {
  type        = any
  description = "Configuracion de restricciones geograficas"
  default     = {}
}

variable logging_config {
  type        = any
  description = "Configuracion de logs que controla como los son escritos."
  default     = {}
}

variable tags {
  type        = map(string)
  description = "Mapa de tags para asignar a cloudfront"
  default     = null
}
