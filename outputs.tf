output "this_cloudfront_distribution_id" {
  description = "Id de la distribucion de cloudfront"
  value       = module.cloudfront.this_cloudfront_distribution_id
}

output "this_cloudfront_distribution_arn" {
  description = "Arn de la distribucion de cloudfront"
  value       = module.cloudfront.this_cloudfront_distribution_arn
}

output "this_cloudfront_distribution_caller_reference" {
  description = "Valor interno usado por cloudfront para permitir futuras actualizacion en la configuracion de la distribucion."
  value       = module.cloudfront.this_cloudfront_distribution_caller_reference
}

output "this_cloudfront_distribution_status" {
  description = "Estatus actual de la distribucion de cloudfront"
  value       = module.cloudfront.this_cloudfront_distribution_status
}

output "this_cloudfront_distribution_trusted_signers" {
  description = "Lista anidada de atributos para firmadores activos."
  value       = module.cloudfront.this_cloudfront_distribution_trusted_signers
}

output "this_cloudfront_distribution_domain_name" {
  description = "Nombre de dominio correspondiente a esta distribucion de cloudfront"
  value       = module.cloudfront.this_cloudfront_distribution_domain_name
}

output "this_cloudfront_distribution_last_modified_time" {
  description = "Fecha de la ultima modificacion"
  value       = module.cloudfront.this_cloudfront_distribution_last_modified_time
}


output "this_cloudfront_distribution_etag" {
  description = "Version actual de la informacion de la distribucion de cloudfront."
  value       = module.cloudfront.this_cloudfront_distribution_etag
}

output "this_cloudfront_distribution_hosted_zone_id" {
  description = "Zone ID de ruta 53 de la distribucion de cloudfront. esta puede ser usado para configurar un alias"
  value       = module.cloudfront.this_cloudfront_distribution_hosted_zone_id
}

output "this_cloudfront_origin_access_identities" {
  description = "Origin access indentities creados"
  value       = module.cloudfront.this_cloudfront_origin_access_identities
}

output "this_cloudfront_origin_access_identity_ids" {
  description = "Id de los origin access indentities creados"
  value       = module.cloudfront.this_cloudfront_origin_access_identity_ids
}

output "this_cloudfront_origin_access_identity_iam_arns" {
  description = "ARN de los iam pertencientes a los origin access indentities creados"
  value       = module.cloudfront.this_cloudfront_origin_access_identity_iam_arns
}
