# Modulo de terraform para crear distribuciones de cloudfront en aws


Este modulo esta basado en el modulo oficial de terraform para la creacion de distribuciones de cloudfront en aws: https://registry.terraform.io/modules/terraform-aws-modules/cloudfront/aws/1.5.0 


### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| price_class  | Clase de precio del cloudfront. PriceClass_All, PriceClass_200, PriceClass_100  | SI  | N/A  |
| default_root_object  | Ruta default a cargar en el bucket  | SI  | N/A  |
| origin  | Configuracion del origin  | SI  | N/A  |
| default_cache_behavior  | Comportamiento por default del cache  | SI  | N/A  |
| ordered_cache_behavior  | Orden del comportamiento  | NO  | []  |
| aliases  | Aliases (CNAMES) para acceder al cloudfront  | NO  | null  |
| comment  | Comentario referente al cloudfront  | NO  | ""  |
| enabled  | Habilitar cloudfront  | NO  | true  |
| is_ipv6_enabled  | Habilitar ipv6  | NO  | true  |
| retain_on_delete  | Mantener en eliminacion  | NO  | false  |
| wait_for_deployment  | Esperar por el deploy  | NO  | false  |
| viewer_certificate  | Configuracion del certificado  | NO  | { cloudfront_default_certificate = true minimum_protocol_version       = "TLSv1" }  |
| create_origin_access_identity  | Crear origin access identity  | NO  | false  |
| origin_access_identities  | Origin access identities  | NO  | {}  |
| geo_restriction  | Configuracion de restricciones geograficas  | NO  | {}  |
| logging_config  | Configuracion de logs que controla como los son escritos  | NO  | {}  |
| tags  | Mapa de tags para asignar a cloudfront  | NO  | null  |


### Outputs

| Nombre  | Descripción  |
|---|---|
| this_cloudfront_distribution_id  | Id de la distribucion de cloudfront  |
| this_cloudfront_distribution_arn  | Arn de la distribucion de cloudfront  |
| this_cloudfront_distribution_caller_reference  | Valor interno usado por cloudfront para permitir futuras actualizacion en la configuracion de la distribucion  |
| this_cloudfront_distribution_status  | Estatus actual de la distribucion de cloudfront  |
| this_cloudfront_distribution_trusted_signers  | Lista anidada de atributos para firmadores activos  |
| this_cloudfront_distribution_domain_name  | Nombre de dominio correspondiente a esta distribucion de cloudfront  |
| this_cloudfront_distribution_last_modified_time  | Fecha de la ultima modificacion  |
| this_cloudfront_distribution_etag  | Version actual de la informacion de la distribucion de cloudfront  |
| this_cloudfront_distribution_hosted_zone_id  | Zone ID de ruta 53 de la distribucion de cloudfront. esta puede ser usado para configurar un alias  |
| this_cloudfront_origin_access_identities  | Origin access indentities creados  |
| this_cloudfront_origin_access_identity_ids  | Id de los origin access indentities creados  |
| this_cloudfront_origin_access_identity_iam_arns  | ARN de los iam pertencientes a los origin access indentities creados  |